
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] private GameObject _prefab;
    [SerializeField] private int _startCount;

    public static Pool Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        for (int i = 0; i < _startCount; i++)
        {
            GameObject newGo = Instantiate(_prefab, transform);
            newGo.gameObject.SetActive(false);
        }

    }

    private Stack<GameObject> _objects;

    public void Add(GameObject go)
    {
        go.SetActive(false);
        _objects.Push(go);
    }
    public GameObject Get()
    {
        
        return _objects.Pop();
    }

}

