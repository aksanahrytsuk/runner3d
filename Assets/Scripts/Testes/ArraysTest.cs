using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArraysTest : MonoBehaviour
{
    public class Info
    {
        public int Id;
        public string Name;
    }

    private void Awake()
    {
        TestArray();
    }

    private void TestList()
    {
        var list = new List<Info>();
        for (int i = 0; i < 10; i++)
        {
            list.Add(new Info() { Id = i, Name = "Name" + i });
        }
        print(list[5].Name);
        //удалить элемент под индексом 5
        list.RemoveAt(5);
        print(list[5].Name);
        var list2 = new List<Info>();
    }

    private void TestQueue()// FIFO
    {
        var queue = new Queue();
        for (int i = 0; i < 10; i++)
        {
            queue.Enqueue(new Info() { Id = i, Name = "Name" + i });
        }

        var item = queue.Peek();
        //Debug.Log(item.Name);
        var item1 = queue.Dequeue();
    }

    private void TestArray()
    {
        var array = new SystemInfo[10];
        for (int i = 0; i < array.Length; i++)
        {

        }
    }
}
