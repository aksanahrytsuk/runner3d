using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastController : MonoBehaviour
{
    [SerializeField] private LayerMask _rayLayerMask;
[SerializeField] CapsuleCollider _capsulCollider;
    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray();
        //луч с позиции объекта
        ray.origin = transform.position;
        //по направлению оси z - вперед
        ray.direction = transform.forward;
        if (Physics.Raycast(ray, out RaycastHit hit, 30f, _rayLayerMask))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.blue);
            Debug.Log(hit.collider.gameObject.name);
            Debug.Log(ray.direction);
        }

        if (Physics.CapsuleCast(GetDownHimispire(), GetDownHimispire(), 20f, transform.forward, 30f, _rayLayerMask))
        {
            Debug.Log("Collision");
        }

        
    }
    private Vector3 GetTopHimisphire()
    {
        return transform.position + Vector3.up * (_capsulCollider.height - _capsulCollider.radius);
    }
    private Vector3 GetDownHimispire()
    {
        return transform.position + Vector3.up * _capsulCollider.radius;
         
    }
}
