using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInputHandler : MonoBehaviour, IInputHandler
{
    [SerializeField] float sensitive = 0.2f;
    private float _horizontalAxis;
    public float GetHorizontalAxis() => _horizontalAxis;
    
    void Update()
    {
        _horizontalAxis = -Input.GetAxis("Horizontal") * sensitive;
    }
}
