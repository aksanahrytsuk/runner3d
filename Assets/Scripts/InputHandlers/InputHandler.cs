using System.Collections;
using UnityEngine;

public class InputHandler : MonoBehaviour, IInputHandler
{
    [Header("Animations")]
    [SerializeField] private AnimationController _animationController;

    [Header("Settings")]
    private Vector3 _position;
    private float _horizontalAxis;

    private bool _isHeald = false;

    // private PlayerController _playerController;
    // private MoveController _moveController;

    //реализация метода интерфейса
    public float GetHorizontalAxis() => _horizontalAxis;

    private void Start()
    {
        //_moveController = GetComponent<MoveController>();
        //_playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isHeald = true;
            _position = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            _isHeald = false;
            _position = Vector3.zero;
        }
        if (_isHeald)
        {
             _animationController.SetRunTrigger();
            Vector3 offset = _position - Input.mousePosition;
           _horizontalAxis = offset.x / Screen.width;
           _position = Input.mousePosition; //обновление позиции каждый кадр
        }
    }
}
