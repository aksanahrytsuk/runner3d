using UnityEngine;

public class MusicEffectsManager : MonoBehaviour
{
    [SerializeField] private AudioSource _effects;
    [SerializeField] private AudioSource _music;
    [SerializeField]private AudioClip _gameSongs;
    #region Singleton
    public static MusicEffectsManager Instance {get; private set;}
    private void Awake() 
    {
        if (Instance != null)
    {
        Destroy(gameObject);
    }
    else
    {
        Instance = this;
    }
    }
    #endregion

private void Start() 
{
    PlayMusic(_gameSongs);
}

    public void PlaySound(AudioClip audio)
    {
        _effects.PlayOneShot(audio);
    }

    public void PlayMusic(AudioClip audio)
    {
        _music.PlayOneShot(audio);
    }
}
