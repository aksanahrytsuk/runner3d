
using UnityEngine;
using UnityEngine.UI;

public class MoneyCounter : MonoBehaviour
{
    [SerializeField] private Text coinText;
    [SerializeField] private Text _score;
    public int _coinsCount;

    public void CoinsAmount()
    {
        _coinsCount++;
        UpdateCoinsText();
    }
    public void UpdateCoinsText()
    {
        coinText.text = "Coins: " + _coinsCount;
        _score.text = "Coints:" + _coinsCount;
    }
}
