using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject StartScreen;
    [SerializeField] private GameObject GameScreen;
    [SerializeField] private GameObject FallScreen;
    [SerializeField] private GameObject ContinueScreen;

    [SerializeField] AdController _adController;

    private GameObject _currentScreen;

    void Start()
    {
        _currentScreen = StartScreen;
        PlayerController.OnDeath += ShowFallScreen;
        PlayerController.OnFinish += ShowContinueScreen;
    }


    public void ShowStartScreen()
    {
        if (_currentScreen != null)
        {
            _currentScreen.SetActive(false);
        }

        StartScreen.SetActive(true);
        _currentScreen = StartScreen;
    }

    public void ShowGameScreen()
    {
        _currentScreen.SetActive(false);
        GameScreen.SetActive(true);
        _currentScreen = GameScreen;
    }

    public void ShowContinueScreen()
    {
        _adController.ShowInterstition();
        _currentScreen.SetActive(false);
        ContinueScreen.SetActive(true);
        _currentScreen = ContinueScreen;
    }
    public void ShowFallScreen()
    {
        _currentScreen.SetActive(false);
        FallScreen.SetActive(true);
        _currentScreen = FallScreen;
    }
}
