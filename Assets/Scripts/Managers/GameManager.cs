using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Managers")]
    [SerializeField] private UIManager _uiManager;

    [Header("Prefabs")]
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _levelPrefab;
    [SerializeField] private GameObject _cameraObject;

    [Header("EffectPrefabs")]
    [SerializeField] private GameObject wallEffect;
    [SerializeField] private GameObject winEffect;
    
    [Header("References")]
    [SerializeField] private MoneyCounter _moneyCounter;
    [SerializeField] private AdController _adController;

    private GameObject _player;
    private PlayerController _playerController;
    private GameObject _level;
    private Level _levelGenerator;


    private PlayerData _playerData;

    void Start()
    {
        PlayerController.WallEffect += WallEffect;
        PlayerController.WinEffect += WinEffect;
        LoadData();
        ShowMenu();
    }

    private void InstantiatePlayer()
    {
        _cameraObject.transform.parent = null;
        Debug.Log("player was created");
        if (_player != null)
        {
            Destroy(_player.gameObject);
            _player = null;
        }
        //clone prefab
        _player = Instantiate(_playerPrefab, transform);
        _player.transform.localPosition = new Vector3(0f, 0.3f, 0.5f);
        _cameraObject.transform.parent = _player.transform;
        _cameraObject.transform.position = new Vector3(0f, 2f, -7f);

        _playerController = _player.GetComponent<PlayerController>();
    }

    public void InstantiateLevel()
    {
        if (_levelGenerator != null)
        {
            Destroy(_levelGenerator.gameObject);
            _levelGenerator = null;
        }

        _level = Instantiate(_levelPrefab, transform);
        _levelGenerator = _level.GetComponent<Level>();
        _levelGenerator.Generate(_playerData.LevelIndex);
    }

    private void LoadData()
    {
        //существует ли PlayerData,создать new PLayerData 
        if (PlayerPrefs.HasKey("PlayerData") == false)
        {
            _playerData = new PlayerData();
            return;
        }
        string playerDataToString = PlayerPrefs.GetString("PlayerData");
        //десериализация из Json format в playerData
        _playerData = JsonUtility.FromJson<PlayerData>(playerDataToString);
    }

    private void SaveData()
    {
        //serialize to Json
        string stringData = JsonUtility.ToJson(_playerData);
        PlayerPrefs.SetString("PlayerData", stringData);
    }

    public void ShowMenu()
    {
        _uiManager.ShowStartScreen();
    }

    public void ReStart()
    {
        _uiManager.ShowGameScreen();
        InstantiatePlayer();
        InstantiateLevel();
    }

    public void NextLevel()
    {
       // _adController.ShowInterstition();
        _uiManager.ShowGameScreen();
        _playerData.LevelIndex++;
        InstantiateLevel();
        InstantiatePlayer();
    }

    public void StopGame()
    {
        _uiManager.ShowContinueScreen();
        SaveData();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void WallEffect()
    {
        if (wallEffect != null)
        {
            Vector3 vfxPosition = _playerController.transform.position;
            GameObject newObject = Instantiate(wallEffect, vfxPosition, Quaternion.identity);
            Destroy(newObject, 3f);
        }
    }

    private void WinEffect()
    {
        if (winEffect != null)
        {
            Vector3 vfxPsition = _playerController.transform.position;
            GameObject newObject = Instantiate(winEffect, vfxPsition, Quaternion.identity);
            Destroy(newObject, 3f);
        }
    }
}


