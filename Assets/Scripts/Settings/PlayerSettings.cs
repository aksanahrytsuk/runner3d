using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Settings", menuName = "Configs/Player setting")]
public class PlayerSettings : ScriptableObject
{
    [SerializeField]  private MovementSettings _movementSettings;

    public MovementSettings MovementSettings => _movementSettings;
}

[System.Serializable]
public class MovementSettings
{
    [SerializeField] private float _forwardSpeed;
    [SerializeField] private float _roadWidth;

    public float ForwardSpeed => _forwardSpeed;
    public float RoadWidth => _roadWidth;

}
