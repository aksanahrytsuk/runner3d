
using UnityEngine;

public class Level : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private float _roadLength;
    // min расстояние между стенами по Z
    [SerializeField] private float _minObstacleOffsetZ;
    // mах расстояние между стенами по Z
    [SerializeField] private float _maxObstacleOffsetZ;
    [SerializeField] private float _startObstPoint;
    [SerializeField] private float _startCoinPoint;
    const float localPositionY = 0.95f;


    [Header("Elements Settings")]
    //длина блока дороги
    [SerializeField] private float _roadPartLength;
    //ширина блока дороги
    [SerializeField] private float _roadPartWidth;



    [Header("Prefabs")]
    [SerializeField] private GameObject _finishPrefab;
    [SerializeField] private GameObject _obstaclePrefab;
    [SerializeField] private GameObject _roadPartPrefab;
    [SerializeField] private GameObject _coinPrefab;

    [SerializeField] LayerMask _lMask;


    [ContextMenu("Generate road")]

    public void Generate(int levelIndex)
    {
        Random.InitState(levelIndex);
        GanerateRoad();
        GenerateObstacle();
        GenerateCoin();
    }

    private void GanerateRoad()
    {
        // текущая позиция в начале локальной системы коррдинат
        Vector3 localPosition = Vector3.zero;
        //длину дороги на длину блока дороги 
        int partCount = Mathf.CeilToInt(_roadLength / _roadPartLength);

        //создать столько префабов _roadPartPrefab < чем partCount
        for (int i = 0; i < partCount; i++)
        {
            //создать префаб блока дороги,  transform (дочерний объект левела)
            GameObject roadPart = Instantiate(_roadPartPrefab, transform);
            roadPart.transform.localPosition = localPosition;
            //создавать каждый блок дороги на расстоянии roadPartLength
            localPosition.z += _roadPartLength;
        }
        //последний префаб дороги - финиш-блок
        GameObject finishPart = Instantiate(_finishPrefab, transform);
        finishPart.transform.localPosition = localPosition;
    }

    private void GenerateObstacle()
    {
        //длина линии блоков равна длине дороги
        float roadWallLength = _roadLength;
        //позиция по  z внутри уровня(длина дороги, на которой генерируются блоки)
        float currentLength = _startObstPoint;
        // крайнее левое положение внутри уровня по ширине дороги
        float xStartPoint = -_roadPartWidth / 2f;
        float xOffset = _roadPartWidth / 4f;

        while (currentLength < roadWallLength)
        {
            int intposition = Random.Range(0, 4);
            float positionX = xStartPoint + intposition * xOffset;

            Vector3 localPosition = new Vector3(positionX, localPositionY, currentLength);
            GameObject wall = Instantiate(_obstaclePrefab, transform);
            wall.transform.localPosition = localPosition;

            currentLength += Random.Range(_minObstacleOffsetZ, _maxObstacleOffsetZ);
        }
    }
    private int RandomOffset(float currentPoint, float xOffset)
    {
        int dice = Random.Range(0, 4);

        if (dice == 1)
        {
            if (currentPoint - xOffset < -_roadPartWidth / 2f)
            {
                return 1;
            }
            else if (currentPoint + xOffset > _roadPartWidth / 2f)
            {
                return -1;
            }
            else
            {
                return Random.Range(0, 2) * 2 - 1;
            }
        }
        return 0;
    }

    private void GenerateCoin()
    {
        //длина линии блоков равна длине дороги
        float roadWallLength = _roadLength;
        //позиция по  z внутри уровня(длина дороги, на которой генерируются блоки)
        float currentLength = _startCoinPoint;
        // крайнее левое положение внутри уровня по ширине дороги
        // float xStartPoint = -_roadPartWidth / 2f + 1;
        float xStartPoint = _roadPartWidth / 8f * (Random.Range(0, 2) * 2 - 1);
        float xOffset = _roadPartWidth / 4f;
        while (currentLength < roadWallLength)
        {
            Vector3 localPosition = new Vector3(xStartPoint, 0.7f, currentLength);
            Vector3 fwd = transform.TransformDirection(Vector3.forward);
            Collider[] objectsInRadius = Physics.OverlapSphere(localPosition, 0.9f, _lMask);
            if (objectsInRadius.Length != 0 )
            {
                print("There is something in front of the object!");
                if (xStartPoint - xOffset <= -_roadPartWidth / 2f) { localPosition.x += xOffset; }
                else if (xStartPoint + xOffset >= _roadPartWidth / 2f) { localPosition.x -= xOffset; }
                else { localPosition.x += (Random.Range(0, 2) * 2 - 1) * xOffset; }
            }

            GameObject coin = Instantiate(_coinPrefab, transform);
            coin.transform.localPosition = localPosition;

            xStartPoint += RandomOffset(xStartPoint, xOffset) * xOffset;
            currentLength += 2f;
        }
    }
}
