using UnityEngine;

public class Coin : MonoBehaviour, ICollectable, ISound, IDoEffect, IRotate
{
    [Header("Rotations and movements")]
    [SerializeField] private Vector3 _rotationAngel;

    [Header("Sounds")]
    [SerializeField] private AudioSource _effect;
    [SerializeField] private AudioClip _collectCoinSound;
    [Header("ParticlEffects")]
    [SerializeField] private GameObject _collectCoinEffects;

    [SerializeField] private GameObject _gamobject;

    void Update()
    {
        Rotate();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.TryGetComponent(out PlayerController player))
        {
            PLaySound(_collectCoinSound);
            CreateEffect(_collectCoinEffects);
            OnDisable();
        }
    }

    private void OnDisable()
    {
        _gamobject.SetActive(false);
    }

    private void OnEnable()
    {
        gameObject.SetActive(true);
    }

    public void PLaySound(AudioClip clip)
    {
        _effect.PlayOneShot(_collectCoinSound);
        OnEnable();
    }

    public void CreateEffect(GameObject response)
    {
        if (_collectCoinEffects != null)
        {
            Vector3 vfxPsition = transform.position; // инстанциировать в позиции монетки(дописать)
            GameObject newObject = Instantiate(_collectCoinEffects, vfxPsition, Quaternion.identity);
            Destroy(newObject, 3f);
        }
    }

    public void Rotate()
    {
        transform.Rotate(_rotationAngel * (Time.deltaTime / 2), Space.World);
    }
}
