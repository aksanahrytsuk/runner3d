using UnityEngine;
using UnityEngine.UI;

public class RewordedButton : MonoBehaviour
{
   [SerializeField] private Button _button;
   [SerializeField] private AdController _adController;
   private void FixedUpdate() 
   {
      _button.interactable = _adController.IsRewardedReady;
   }
}
