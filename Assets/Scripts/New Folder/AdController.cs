using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdController : MonoBehaviour
{
    // keys - тестовый ключ для андроид, для рекламы можно скопировоть с папки ironesours/demoscene script for android or iOS
    [SerializeField] private string _appKey = "85460dcd";
    private bool _isInterstitialReady;
    private bool _isRewardedReady;

    //true если реворд активен
    public bool IsRewardedReady => _isRewardedReady;

    private void Awake()
    {
        //обязательная инициализация adUnits 
        IronSource.Agent.init(_appKey, IronSourceAdUnits.BANNER, IronSourceAdUnits.INTERSTITIAL, IronSourceAdUnits.REWARDED_VIDEO);
        //подписка на ивенты
        SubscribeEvents();
        // проверяет, успешно ли завершена интеграция с Iron sours sdk или рекламными сетями
        Debug.Log("unity script: IronSource.Agent.validateIntegration.");
        IronSource.Agent.validateIntegration();
    }

    private void SubscribeEvents()
    {
        //покажет проинициализировался ли sdk 
        IronSourceEvents.onSdkInitializationCompletedEvent += InitializeCallback;
        //check errors
        IronSourceEvents.onBannerAdLoadFailedEvent += BannerLoadFaild;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitalLoadFaild;
        IronSourceEvents.onRewardedVideoAdLoadFailedEvent += RewardeLoadFaild;

        // check when interstitial was loaded 
        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialReady;
        // if Interstital is open hid banner 
        IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialOpend;
        // Interstital was closed after view
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialClose;
        
        IronSourceEvents.onRewardedVideoAdReadyEvent += RewardedReady;
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardeOpend;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewordedClose;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardEvent;

    }

    //загрузка units отдельно для каждого элемента
    private void InitializeCallback()
    {
        IronSource.Agent.loadBanner(IronSourceBannerSize.SMART, IronSourceBannerPosition.BOTTOM);
        IronSource.Agent.loadInterstitial();
        IronSource.Agent.loadRewardedVideo();
    }

    // подписка на ошибки крайне рекомендуется!
    private void BannerLoadFaild(IronSourceError error) => Debug.Log(error);
    private void InterstitalLoadFaild(IronSourceError error) => Debug.Log(error);
    private void RewardeLoadFaild(IronSourceError error) => Debug.Log(error);

    #region Interstitial
    //готов к загрузке и к вызову
    private void InterstitialReady() => _isInterstitialReady = true;


    //закрывать после показа, после того, как игрок закрыл Interstitial
    private void InterstitialClose()
    {
        _isInterstitialReady = false;
        IronSource.Agent.displayBanner();
        IronSource.Agent.loadInterstitial();
    }

    private void InterstitialOpend()
    {
        IronSource.Agent.hideBanner();
    }

    //прказать Interstitial, если он готов
    public void ShowInterstition()
    {
        if (_isInterstitialReady == false) return;
        
        IronSource.Agent.showInterstitial();
    }
    #endregion

    #region Rewarde
    private void RewardedReady() => _isRewardedReady = true;
    private void RewordedClose()
    {
        _isRewardedReady = false;
        IronSource.Agent.displayBanner();
        IronSource.Agent.loadRewardedVideo();
    }
    private void RewardeOpend()
    {
        IronSource.Agent.hideBanner();
    }


    // вызов метода, который даёт некий бонус
    private void RewardEvent(IronSourcePlacement ironSourcePlacement)
    {
        Debug.Log(ironSourcePlacement.getRewardName());
    }
    public void ShowRewordedVideo()
    {
        if (_isRewardedReady == false) return;

        IronSource.Agent.loadRewardedVideo();

    }

    #endregion
}
