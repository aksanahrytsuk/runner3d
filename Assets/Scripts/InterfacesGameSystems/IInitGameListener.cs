using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInitGameListener 
{
   void Initialize(GameSystem gameSystem);
}




