using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
   [SerializeField] private List<MonoBehaviour> _listeners;
    private GameState _gameState = GameState.InitGame;
    private void Awake() 
    {
        InitGame();
    }

    public void AddListener (MonoBehaviour monoBehaviour)
    {
        _listeners.Add(monoBehaviour);
        
    }
    public void InitGame()
    {
        for (int i = 0; i < _listeners.Count; i++)
        {
            if (_listeners[i] is IInitGameListener listener)
            {
                listener.Initialize(this);
            }
        }
    }
    public void StartGame()
    {
          for (int i = 0; i < _listeners.Count; i++)
        {
            if (_listeners[i] is IInitGameListener listener)
            {
                //listener.StartGame(this);
            }
        }
    }
    public void PauseGame()
    {
            for (int i = 0; i < _listeners.Count; i++)
        {
            if (_listeners[i] is IInitGameListener listener)
            {
                //listener.StartGame(this);
            }
        }
    }
    public void ResumeGame()
    {
            for (int i = 0; i < _listeners.Count; i++)
        {
            if (_listeners[i] is IInitGameListener listener)
            {
                //listener.StartGame(this);
            }
        }
    }
    public void WinGame()
    {
            for (int i = 0; i < _listeners.Count; i++)
        {
            if (_listeners[i] is IInitGameListener listener)
            {
                //listener.StartGame(this);
            }
        }
    }
    public void FallGame()
    {
            for (int i = 0; i < _listeners.Count; i++)
        {
            if (_listeners[i] is IInitGameListener listener)
            {
                //listener.StartGame(this);
            }
        }
    }
}



public enum GameState
{
    InitGame,
    StartGame,
    PauseGame,
    ResumeGame,
    WinGame,
    FallGame,
}
