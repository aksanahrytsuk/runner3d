using UnityEngine;

public class AnimationController : MonoBehaviour
{
  [SerializeField] private Animator _animator;
  private int _runID;
  private  int _fallID;
  private int _winID;
  private int _idleID;
  
  void Awake()
  {
      _runID = Animator.StringToHash("Run");
      _fallID = Animator.StringToHash("Fall");
      _winID = Animator.StringToHash("Dance");
      _idleID = Animator.StringToHash("Idle");
  }

  public void SetRunTrigger()
  {
      SetTrigger(_runID);
  }

   public void SetFallTrigger()
  {
      SetTrigger(_fallID);
  }
   public void SetDanceTrigger()
  {
      SetTrigger(_winID);
  }

  public void SetIdleTrigger()
  {
      SetTrigger(_idleID);
  }

  public void SetTrigger (int id) => _animator.SetTrigger(id);
  //public void SetTrigger (string triggerName) => _animator.SetTrigger(triggerName);
}
