using System;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Animations")]
    [SerializeField] private AnimationController _animationController;

    [Header("References")]
    [SerializeField] private AudioSource Effect;
    [SerializeField] private AudioClip _bumpSound;

    [SerializeField] private float _dilay;

    private MoneyCounter _moneyCounter;
    private IInputHandler _inputHandler;
    private IMoveController _moveController;

    private Rigidbody _rb;

    private bool _isActive = false;

    public static event Action OnDeath;
    public static event Action OnFinish;
    public static event Action WallEffect;
    public static event Action WinEffect;

    public bool IsActive
    {
        get
        {
            return _isActive;
        }
        set
        {
            if (value == true) _animationController.SetRunTrigger();
            _isActive = value;
        }
    }

    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }
    void Start()
    {
        _isActive = true;
        _animationController.SetRunTrigger();
        _inputHandler = GetComponent<IInputHandler>();
        _moveController = GetComponent<IMoveController>();
        _moneyCounter = FindObjectOfType<MoneyCounter>();
    }

    private void Update()
    {
        if (_isActive == false) return;
        _moveController.Move(_inputHandler.GetHorizontalAxis());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out Wall collide)) //получить компонент указанного типа if he has
        {

            _animationController.SetFallTrigger();
            Died();
        }
        if (other.gameObject.TryGetComponent(out Finish finishBlock))
        {

            _animationController.SetDanceTrigger();
            Win();
        }
        if (other.gameObject.TryGetComponent(out Coin collectCoin))
        {
            _moneyCounter.CoinsAmount();
        }
    }

    private void Died()
    {
        IsActive = false;
        WallEffect?.Invoke();
        MusicEffectsManager.Instance.PlaySound(_bumpSound);
        _rb.rotation = Quaternion.identity;
        _rb.velocity = Vector3.zero;
        // OnDeath?.Invoke();
        StartCoroutine(WaitCoroutin(OnDeath)); //одна самостоятельная корутина(не связана со второй)
    }

    private void Win()
    {
        IsActive = false;
        WinEffect?.Invoke();
        _rb.rotation = Quaternion.identity;
        _rb.velocity = Vector3.zero;
        //OnFinish?.Invoke();
        StartCoroutine(WaitCoroutin(OnFinish)); //вторая самостоятельная корутина
    }
    //Корутина от Monobeheviour
    private IEnumerator WaitCoroutin(Action action)
    {
        yield return new WaitForSeconds(_dilay);
        action?.Invoke();
    }


    // private IEnumerator WaitCoroutine(Action callback, int del) //можно вызывать корутину перед стартом, изображая изобразив отсчет "текст-цифры" 
    // {
    //     {
    //         while (del > 0)
    //         {
    //             Debug.Log(del);
    //             del--;
    //             yield return new WaitForSeconds(1f);
    //         }
    //         callback?.Invoke();
    //     }
    // }
}
