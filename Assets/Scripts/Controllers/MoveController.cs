using UnityEngine;

public class MoveController : MonoBehaviour, IMoveController
{
    [Header("Settings")]

    [SerializeField] PlayerSettings _playerSettings;
    private MovementSettings _settings;
    
    public void Move(float horizontalAxis)
    {
        if (_settings == null)
        {
            _settings = _playerSettings.MovementSettings;
        }
          Vector3 position = transform.position;
        //инвртированное управление без -
        float xoffset = -horizontalAxis * _settings.RoadWidth;
        float zoffset = _settings.ForwardSpeed * Time.deltaTime;

        //управление в пределах ширины дороги
        if (Mathf.Abs(position.x + xoffset) > _settings.RoadWidth / 2f)
        {
            xoffset = (_settings.RoadWidth / 2 - Mathf.Abs(position.x)) * Mathf.Sign(position.x);
        }

        position.x += xoffset;
        position.z += zoffset;
        transform.position = position;
    }

}
